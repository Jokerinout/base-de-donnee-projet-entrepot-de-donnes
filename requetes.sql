-- DataBase project - Leveille Bastien, Lode Gael, Lecomte Soline, Perez Damien

-- 1 - Année de publication, titre et nombre de ventes des dix albums les mieux notés par rolling stones

SELECT * FROM (
    SELECT yearOfPubli, rollingStones, title, numOfSales
    FROM critics NATURAL JOIN facts NATURAL JOIN albums
    WHERE rollingStones = 5)
WHERE ROWNUM <= 10
ORDER BY yearofpubli;

-- 2 - Par année de publication, titre de l’album le plus vendu, avec son classement par rapport aux autres

SELECT ROWNUM, yearOfPubli, idAlbum, title, numOfSales
FROM facts NATURAL JOIN albums
WHERE numOfSales IN(
    SELECT MAX(numOfSales)
    FROM facts
    GROUP BY yearOfPubli)
GROUP BY ROWNUM, yearOfPubli, idAlbum, title, numOfSales
ORDER  BY yearOfPubli DESC;

-- 3 - Nom et lieu de naissance des dix artistes ayant vendus les albums les mieux notés en moyenne par les trois critiques (avec les noms des albums)

SELECT * FROM (
    SELECT (rollingStones + mtv + musicManiac) /3 AS average, title, artistName, realname, country
    FROM critics NATURAL JOIN facts NATURAL JOIN artists NATURAL JOIN albums
    ORDER  BY average DESC)
WHERE ROWNUM<= 10;

-- 4 - Top 10 des artistes les plus jeunes à avoir sorti un album

SELECT * FROM (
    SELECT (yearOfPubli-yearOfBirth) AS age, artistName, realName, yearOfBirth, yearOfPubli
    FROM facts NATURAL JOIN artists NATURAL JOIN albums
    ORDER  BY age ASC)
WHERE ROWNUM <= 10;

-- 5 - Top 10 des artistes les plus vieux à avoir sorti un album

SELECT * FROM (
    SELECT (yearOfPubli-yearOfBirth) AS age, artistName, realName, yearOfBirth, yearOfPubli
    FROM facts NATURAL JOIN artists NATURAL JOIN albums
    ORDER  BY age DESC)
WHERE ROWNUM <= 10;

-- 6 - Par année, moyenne d’âge des artistes ayant sorti un album

SELECT yearOfPubli, AVG(yearOfPubli - yearOfBirth) AS moyenneAge
FROM facts NATURAL JOIN artists NATURAL JOIN albums
WHERE yearOfPubli >= 2000 AND yearOfPubli <= 2020
GROUP BY CUBE (yearOfPubli);

-- 7 - Par année, âge minimum des artistes ayant sorti un album

SELECT yearOfPubli, MIN(yearOfPubli - yearOfBirth) AS ageMin
FROM facts NATURAL JOIN albums NATURAL JOIN artists
WHERE yearOfPubli >=2000 AND yearOfPubli <=2020
GROUP BY yearOfPubli
ORDER  BY yearOfPubli;

-- 8 - Top 20 des nationalités les plus fréquentes chez les artistes

SELECT * FROM(
    SELECT COUNT(country) AS nbcountry, country
    FROM  artists
    GROUP BY country
    ORDER  BY COUNT(country) DESC)
WHERE ROWNUM <= 20 ;

-- 9 - Par année, nombre moyen de tracks parmis les albums les plus vendus

SELECT AVG(numberOfTracks) AS averageStracks, yearOfPubli
FROM facts NATURAL JOIN albums
WHERE numOfSales in (
    SELECT MAX(numOfSales)
    FROM facts
    GROUP BY yearOfPubli)
GROUP BY yearOfPubli
ORDER  BY yearOfPubli

-- 10 - Top 5 des artistes ayant sorti le plus d’albums

SELECT * FROM (
    SELECT COUNT(idAlbum) AS nbAlbum, artistName, realNAme, title
    FROM facts NATURAL JOIN artists NATURAL JOIN albums
    GROUP BY (artistName,realName, title)
    ORDER  BY nbAlbum DESC)
WHERE ROWNUM <=5;
