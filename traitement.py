# DataBase project - Leveille Bastien, Lode Gael, Lecomte Soline, Perez Damien
# coding=utf-8

import pandas as pd

dataAlbum = pd.read_csv('albums.csv')
dataArtiste = pd.read_csv('artists.csv')

fichier = open("config.sql", "a")

fichier.write("\n")

# insertions table artists, si la donnée artistName est vide, on la met à Null
for x in range (0, dataArtiste.shape[0]):
    if(pd.notnull(dataArtiste["art_name"][x])) : # si l'artste a un nom de scène différent de Nan
        fichier.write("INSERT INTO artists (artistName,realName,role,yearOfBirth,email,country,city,zipCode) VALUES ('" + str(dataArtiste["art_name"][x]) + "','" + str(dataArtiste["real_name"][x]) + "','" + str(dataArtiste["role"][x]) + "'," + str(dataArtiste["year_of_birth"][x]) + ",'" + str(dataArtiste["email"][x]) + "','" + str(dataArtiste["country"][x]) + "','" + str(dataArtiste["city"][x]) + "','" + str(dataArtiste["zip_code"][x]) + "');")
    else :
        fichier.write("INSERT INTO artists (artistName,realName,role,yearOfBirth,email,country,city,zipCode) VALUES (NULL,'" + str(dataArtiste["real_name"][x]) + "','" + str(dataArtiste["role"][x]) + "'," + str(dataArtiste["year_of_birth"][x]) + ",'" + str(dataArtiste["email"][x]) + "','" + str(dataArtiste["country"][x]) + "','" + str(dataArtiste["city"][x]) + "','" + str(dataArtiste["zip_code"][x]) + "');")
    fichier.write("\n")
fichier.write("\n")

# insertions table albums
for y in range (0, dataAlbum.shape[0]):
    fichier.write("INSERT INTO albums (title,genre,numberOfTracks) VALUES ('" + str(dataAlbum["album_title"][y]) + "','" + str(dataAlbum["genre"][y]) + "'," + str(dataAlbum["num_of_tracks"][y]) + ");")
    fichier.write("\n")
fichier.write("\n")

# insertionss table critics
for z in range (0, dataAlbum.shape[0]):
    fichier.write("INSERT INTO critics (rollingStones,mtv,musicManiac) VALUES (" + str(dataAlbum["rolling_stone_critic"][z]) + "," + str(dataAlbum["mtv_critic"][z]) + "," + str(dataAlbum["music_maniac_critic"][z]) + ");")
    fichier.write("\n")
fichier.write("\n")

# insertions table facts
for k in range (0, dataAlbum.shape[0]):
    fichier.write("INSERT INTO facts (idArt,idAlb,idCrit,yearOfPubli,numOfSales) VALUES (" + str(dataAlbum["artist_id"][k]) + "," + str(dataAlbum["id"][k]) + "," + str(k+1) + "," + str(dataAlbum["year_of_pub"][k]) + "," + str(dataAlbum["num_of_sales"][k]) + ");")
    fichier.write("\n")
fichier.write("\n")

print("Insertion done without problems")

fichier.close()
