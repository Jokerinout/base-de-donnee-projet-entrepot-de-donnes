Bases De Données evoluées - Entrepôt de Données

-- Leveille Bastien, Lode Gael, Lecomte Soline, Perez Damien --

Le but de ce projet était de créér un entrepôt de Données, et d'effectuer des requêtes avec des opérateurs OLAP sur cet entrepôt.

-----------------------------------------------------

Le script traitement.py remplit un fichier config.sql, si le fichier n'est pas présent dans le dossier il le créér.
Attention le fichier remplit uniquement les lignes "INSERT INTO table" il ne créér pas les tables, il faut penser à effectuer la création
des tables en entête du fichier, le script écrira à la suite.

Ce script utilise la librairie Pandas qui doit être au préalable installée.

Pour éxécuter le fichier il suffit d'aller dans le dossier et de faire la commande :

python traitement.py

Le fichier résultant config.sql est le fichier avec la syntaxe de MySQL, pour savoir pourquoi nous avons changé de client SQL par la suite il faut voir le rapport associé.

-----------------------------------------------------

Les fichiers CSV sont les données récupérées depuis le dataset suivant :

https://www.kaggle.com/revilrosa/music-label-dataset#artists.csv

Ils définissent respectivement les artistes et les albums.

-----------------------------------------------------

Enfin le fichier configOraclev2.sql est le deuxième fichier de configuration de la base. C'est le fichier avec moins de lignes de données et la syntaxe de Oracle.
